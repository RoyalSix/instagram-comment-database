const { promisify } = require('util');
var kill = require('tree-kill');
var tor = require('tor-request');
var { sleep } = require('sleep');
var { spawn, execSync } = require('child_process');
const publicIp = require('public-ip');
const firebase = require('../').firebase;
const request = require('request');
const botWordList = require('./static/botWordList');
const prepositionList = require('./static/prepositionList');
var database = firebase.database();
const MIN_CHARACTER_REPEAT_FOR_BAD_COMMENT = 3;
const MIN_WORD_REPEAT_FOR_BAD_COMMENT = 3;
const MIN_WORD_AMOUNT_FOR_BAD_COMMENT = 3;
var asyncRequest = promisify(tor.request);


module.exports = {
  hasGoodFollowRatio: function (followingAmount, followerAmount, numberOfPosts) {
    var self = this;
    let reason = [];
    let is_celebrity, is_fake_account, is_active_user;
    if ((followerAmount / followingAmount) > 2) {
      is_celebrity = true
      is_fake_account = false
      reason.push('is celebrity ');
    }
    else if ((followingAmount / followerAmount) > 2) {
      is_fake_account = true
      is_celebrity = false
      reason.push('is fake account');
    }
    else {
      is_celebrity = false
      is_fake_account = false
      reason.push('is nomal user');
    }
    if ((followingAmount / numberOfPosts < 10) && (followerAmount / numberOfPosts < 10)) {
      is_active_user = true
      reason.push('is active user');
    }
    else {
      is_active_user = false
      reason.push('is not active user');

    }
    reason.join(' ');
    let shouldPerform = (!is_fake_account && is_active_user) || is_celebrity;
    return {
      shouldPerform,
      reason
    }
  },
  initializeTorClient: function () {
    var self = this;
    return new Promise(async (resolve, reject) => {
      const tor_request = tor.request;
      await this.nonBlockingSleep(1);
      console.log('Starting tor client')
      var child;
      if (process.platform === "win32") {
        child = spawn(
          'CMD', [
            '/S',
            '/C',
            'start',
            '/B',
            './tor-lib/Tor/tor.exe'
          ]
        );
      } else {
        try {
          execSync('killall tor');
        } catch (e) { }
        await this.nonBlockingSleep(5);
        child = spawn('tor');
      }
      child.stdout.on('data', async (data) => {
        let dataString = data.toString();
        console.log(dataString)
        if (dataString.includes('100%')) {
          console.log('Tor is done, making sure ip is hidden')
          await self.nonBlockingSleep(3);
          tor_request.get('https://api.ipify.org', async function (err, res, torIP) {
            console.log('Got new IP from ipify')
            await self.nonBlockingSleep(2);
            if (!err && res.statusCode == 200) {
              await self.nonBlockingSleep(1);
              let ip = await publicIp.v4();
              if (torIP !== ip) {
                console.log('Tor is working public IP hidden')
                resolve()
              }
              else reject('There was an error with tor')
            } else {
              console.log(err, res)
            }
          })
        }
      })
    })
  },
  randomIntFromInterval: function (min, max, fast = false) {
    if (fast) return 0;
    return Math.floor(Math.random() * (max - min + 1) + min);
  },
  getBayesImage: async function (username, key) {
    let snap = await database.ref(`users/${this.filterUsername(username)}/bot/settings/${key}`).once('value')
    return snap.val()
  },
  addCommentData: async function (key, data) {
    return await database.ref(`comments/data/${this.filterPathForFirebase(key)}`).push(data);
  },
  filterPathForFirebase: function (name) {
    return name.replace(/\.|\#|\[|\]|\$/ig, '_');
  },
  postLastComments: async function (postID) {
    let snap = await database.ref(`comments/index/${postID}`).once('value');
    return snap.val();
  },
  savePost: async function (postID, nextPage) {
    return await database.ref(`comments/index/${postID}`).set(nextPage);
  },
  containsProfanity: async function (text) {
    return new Promise((resolve, reject) => {
      request.get(`http://www.purgomalum.com/service/containsprofanity?text=${text}`, (err, res, body) => {
        let result = false;
        try {
          result = JSON.parse(body);
        } catch (e) { }
        resolve(result);
      })
    })
  },
  isCommentBoty: function (comment) {
    //let hasBotyWords = this.checkForBotyWords(comment);
    let hasRepeatedCharacters = this.checkForRepeatedCharacters(comment);
    let hasRepeatedWords = this.checkForRepeatedWords(comment);
    //let hasLowAmountOfWords = this.checkForLowWords(comment);
    let prediction = (hasRepeatedCharacters + hasRepeatedWords) / 2;
    return prediction > .5
  },
  checkForBotyWords: function (comment) {
    let botWordsAmount = botWordList.filter((word) => {
      var regex = new RegExp(word);
      return comment.match(regex);
    }).length;
    if (botWordsAmount === 0) return .35;
    return .6 * botWordsAmount / 1.3 + Math.pow(.2, botWordsAmount);
  },
  checkForRepeatedCharacters: function (comment) {
    let repeatedEmojis = 0;
    let emojisArray = this.separateByEmojis(comment);
    if (emojisArray) {
      repeatedEmojis = emojisArray.filter((emoji, index) => {
        return emojisArray[index + 1] === emoji || emojisArray[index - 1] === emoji;
      }).length;
    }
    let repeatedCharacters = comment.split(' ').filter((word) => {
      return word.split('').filter((char, index) => {
        return word[index + 1] === char || word[index - 1] === char;
      }).length >= MIN_CHARACTER_REPEAT_FOR_BAD_COMMENT;
    }).length + repeatedEmojis;
    if (repeatedCharacters === 0) return .35;
    return .5 * repeatedCharacters / 1.1;
  },
  checkForRepeatedWords: function (comment) {
    let repeatedWordsAmount = comment.split(/\W/).filter((word) => {
      if (prepositionList.includes(word)) return false;
      var regex = new RegExp('^' + word + '$', 'gi');
      return (comment.match(regex) || []).length >= MIN_WORD_REPEAT_FOR_BAD_COMMENT;
    }).length;
    if (repeatedWordsAmount === 0) return .5;
    return .6 * repeatedWordsAmount / 1.5 + Math.pow(.2, repeatedWordsAmount);
  },
  checkForLowWords: function (comment) {
    let wordLength = (comment.split(" ") || []).length;
    if (wordLength > MIN_WORD_AMOUNT_FOR_BAD_COMMENT) return .5;
    return .6 * 1 / wordLength * 1.7 - Math.pow(.2, wordLength)
  },
  separateByEmojis: function (comment) {
    return comment.match(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g)
  },
  randomWithProbability: function (objectOfNumbers) {
    var arryOfNumbers = [];
    for (var key in objectOfNumbers) {
      let counter = 0;
      let total = objectOfNumbers[key]
      while (counter < total) {
        arryOfNumbers.push(key)
        counter++
      }
    }
    var idx = Math.floor(Math.random() * arryOfNumbers.length);
    return arryOfNumbers[idx];
  },
  nonBlockingSleep: function (sleepTime) {
    return new Promise((resolve, reject) => {
      setTimeout(() => { return resolve() }, sleepTime * 1000);
    })
  },
  shuffleArray: function (array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  },
  extractWordsFromString: function (text) {
    return text.split(' ').map((word) => {
      return word.split('').filter((char) => char.match(/[a-z]|[A-Z]/)).join('')
    }).join(' ');
  },
  isEnlgish: function (comment) {
    return new Promise(async (resolve, reject) => {
      let englishWords = 0;
      let wordsExtracted = this.extractWordsFromString(comment);
      let textArray = wordsExtracted.split(' ');
      let total = textArray.length;
      for (text of textArray) {
        let { statusCode } = await asyncRequest(`http://www.dictionary.com/browse/${text}`);
        if (statusCode === 200) englishWords++;
      }
      return resolve((englishWords / total) > .5);
    });
  }
}