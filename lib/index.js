var { spawn, exec } = require('child_process');
var { setInterval } = require('timers');

var { firebase } = require('../');
var bayes = require('bayes');
var { Bot, Instagram } = require('instagram-api-node');
var api = new Instagram('petsoftheday_ig', 'Thehundreds');
var utils = require('./utils');
var tor = require('tor-request');

var hashtags = ['love', 'instagood', 'photooftheday', 'fashion', 'beautiful', 'happy', 'cute', 'tbt'];
var hashtags_const = hashtags.slice(0);
var classifier = bayes();
// Require the client
const Clarifai = require('clarifai');
const CLARIFAI_API_KEY = 'a0bd39850f9940cb96b245d750e98260'

async function getUserInfo(username, get) {
  var userObject = await api.getUserInfo(username, get);
  if (!userObject) return {};
  var followers = userObject.user.followed_by.count;
  var following = userObject.user.follows.count;
  var postsAmount = userObject.user.media.count;
  return { followers, following, postsAmount }
}

async function getComments(post, lastCommentsPage) {
  var sleepTime = utils.randomIntFromInterval(1, 20);
  console.log(`Sleeping for ${sleepTime} secs`);
  await utils.nonBlockingSleep(sleepTime);
  let { data } = await api.getMediaComments(post.code || post.shortcode, lastCommentsPage);
  var { edges, page_info } = data && data.shortcode_media &&
    data.shortcode_media.edge_media_to_comment ? data.shortcode_media.edge_media_to_comment
    : {};
  return { comments: edges, nextPage: page_info && page_info.has_next_page ? page_info.end_cursor : false };
}

async function filterComments(comments) {
  if (comments) {
    console.log(`Got ${comments.length} comments for post`);
    console.log('Filtering out bad comments');
    comments = await getGoodCandidateComments(comments);
    return comments.filter((comment) => {
      return comment && comment.node && comment.node.text
    })
  }
}

async function getGoodCandidateComments(comments, censored = true) {
  var filtered = [];
  console.log('Starting comment analysis...this may take a while');
  for (var comment of comments) {
    let commentText = comment.node.text;
    let username = comment.node.owner.username;
    console.log(`got comment ${commentText} from ${username}`);
    if (commentText.match(/#|@/)) {
      console.log(`comment: ${commentText} has a hashtag or tag`)
      continue;
    }
    var sleepTime = utils.randomIntFromInterval(5, 30);
    console.log('sleeping for', sleepTime, 'secs');
    await utils.nonBlockingSleep(sleepTime);
    let goodUser = true;

    goodUser = goodUser && !utils.isCommentBoty(commentText);
    if (!goodUser) {
      console.log(`comment: ${commentText} identified as being boty not using.`)
      continue;
    }
    console.log('comment is not boty')

    let isEnlgishComment = await utils.isEnlgish(commentText);
    goodUser = goodUser && isEnlgishComment;
    if (!goodUser) {
      console.log(`comment: ${commentText} identified as not English not using.`)
      continue;
    }
    console.log('comment is english')

    if (censored) {
      let profane = await utils.containsProfanity(commentText);
      goodUser = goodUser && !profane;
      if (!goodUser) {
        console.log(`comment: ${commentText} identified as containing profanity not using`)
        continue;
      }
      console.log('comment is clean')
    };

    // var { postsAmount, followers, following } = await getUserInfo(username, tor.request);
    // if (!postsAmount || !followers || !following) continue;
    // let { shouldPerform, reason } = utils.hasGoodFollowRatio(following, followers, postsAmount);
    // goodUser = goodUser && shouldPerform;
    // if (!goodUser) {
    //   console.log(`user: ${username} identified as a ${reason}`)
    //   continue;
    // }
    // console.log('commenter has good follow ratio')

    console.log('Got good comment', commentText, 'from user', username);
    filtered.push(comment);
  }
  return filtered;
}

async function getPosts(tag) {
  console.log('Getting posts for tag:', tag);
  let items;
  while (!items) {
    let postObjects = await api.getTopHashtagFeed(tag, null, tor.request);
    items = postObjects.items;
  }
  var posts = items.filter((post) => {
    return !!post.edge_media_to_comment.count;
  })
  console.log(`Got ${posts.length} posts for tag ${tag}`)
  var sleepTime = utils.randomIntFromInterval(5, 30);
  console.log(`Sleeping for ${sleepTime} secs`);
  await utils.nonBlockingSleep(sleepTime);
  return posts;
}

async function getPredictions(post) {
  console.log('Learning from all good comments');
  const app = new Clarifai.App({
    apiKey: CLARIFAI_API_KEY
  });
  let { outputs: [{ data: { concepts } }] } = await app.models.predict(Clarifai.GENERAL_MODEL, post.thumbnail_src);
  let sorted = concepts.sort((a, b) => { return b.value - a.value });
  return sorted.map(({ name, value }) => { return { name: name, value: value * 10 } })
    .splice(0, 5);
}

function getElementPath(predictions) {
  return predictions.map(({ value, name }) => {
    value = parseInt(value * 100);
    return `${name}/${value}`;
  }).join("/");
}

async function shouldActOnPost(post) {
  let lastCommentPage = await utils.postLastComments(post.id);
  if (lastCommentPage === false) return false;
  if (lastCommentPage === null) return '';
  if (typeof (lastCommentPage) === 'string') return lastCommentPage;
}

async function savePost(post, nextPage) {
  return await utils.savePost(post.id, nextPage);
}

async function learnFromPost(post, tag) {
  let actOnPost = await shouldActOnPost(post);
  if (actOnPost === false) return;
  let { comments, nextPage } = await getComments(post, actOnPost);
  let filteredComments = await filterComments(comments);
  let predictions = await getPredictions(post);
  for (var comment of filteredComments) {
    let pathToEntry = getElementPath(predictions);
    await utils.addCommentData(pathToEntry, comment);
    await savePost(post, nextPage);
  }
}

async function learnFromPosts(posts, tag) {
  for (var post of posts) {
    let count = post.edge_media_to_comment ? post.edge_media_to_comment.count : post.comments ? post.comments.count : null;
    if (count) {
      let prob = {
        post: 70,
        commenter: 30
      }
      let chosenType = utils.randomWithProbability(prob);
      if (chosenType === 'post') {
        console.log(`Using post to learn, check it out at https://www.instagram.com/p/${post.code || post.shortcode}/`);
        await learnFromPost(post, tag);
      } else {
        var sleepTime = utils.randomIntFromInterval(60, 360);
        console.log(`Sleeping for ${sleepTime} secs`);
        await utils.nonBlockingSleep(sleepTime);
        let { comments, nextPage } = await getComments(post);
        let chosenCommenter = comments[utils.randomIntFromInterval(0, comments.length - 1)];
        console.log(`Using user ${chosenCommenter.node.owner.username}'s feed to learn`)
        let commenterFeed = await api.getUserFeed(chosenCommenter.node.owner.username);
        if (commenterFeed && commenterFeed.items) {
          await learnFromPosts(commenterFeed.items, tag);
        }
      }
    }
  }
}
async function start() {
  //await utils.initializeTorClient();
  hashtags = utils.shuffleArray(hashtags);
  for (var tag of hashtags) {
    let posts;
    while (!posts) {
      posts = await getPosts(tag);
    }
    await learnFromPosts(posts, tag);
    let index = hashtags.indexOf(tag);
    if (index !== -1) hashtags.splice(index, 1);
    else console.error('Something went wrong', console.trace())
  }
  if (!hashtags.length) hashtags = hashtags_const.slice(0);
}
start()
setInterval(start, utils.randomIntFromInterval(1800 * 1000, 3600 * 1000))